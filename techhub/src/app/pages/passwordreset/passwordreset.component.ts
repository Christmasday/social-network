import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-passwordreset',
  templateUrl: './passwordreset.component.html',
  styleUrls: ['./passwordreset.component.css']
})
export class PasswordresetComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onResetPassword(reset: NgForm) {
    console.log(reset.value);
    reset.reset();
  }

}
