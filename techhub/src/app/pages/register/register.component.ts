import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../_models/user';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    @ViewChild ('registerForm')  form: any;

  user: User = {
    firstname: '',
    lastname: '',
    username: '',
    email: '',
    password: ''

  };

  constructor() { }

  ngOnInit() {
  }

  onRegister(registerForm: NgForm) {
    console.log(registerForm.value);
    registerForm.resetForm();
  }

}
