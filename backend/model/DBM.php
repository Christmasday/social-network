<?php
include('../model/nosql.php');
include('../model/sql.php');
Trait Manager
{
    use DBManager;
    static $map = array(),
                   $response;

    final static function init()
    {
        $type = DB_TYPE;
        return self::$type();
    }
    
    final static function Search()
    {
        DBManager::connect();
        $response = Response::json(200, DBManager::Search());
        DBManager::disconnect();
        return $response;
    }

    final static function nosql()
    {
        return nosql::requestinit();
    }

    final static function sql()
    {
        return sql::requestinit();
    }
}

Trait AuthModel
{
    use Manager;
    
    final function validate($pass = null, $auth = null, $salt = null) 
    {
        $password = $auth['password'];
        unset($auth['password']);
        unset($auth['id']);
        return ($password !== _create_hash($pass, $salt))
                ? Response::json(401, "Incorrect username or password")
                : Response::json(200, 
                    array(
                        "token" => Token::generateToken(@ $auth['access'], @ $GLOBALS['HEADERS']['X-Token']), 
                        "data" => $auth,
                    )
                );
    }

    final static function getAuthData($arr = array())
    {
        DBManager::$table = strtolower(DBManager::$table."auth");
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $_REQUEST['controller'] = $_GET['controller'] = 'accounts';
        foreach(DBManager::$key as $index => $values)
        {
            $_GET['controller'] .= '/'.$values.'/'.$arr[$values];
        }
        $_REQUEST['controller'] = $_GET['controller'];
        $user = json_decode(Manager::init(), TRUE)['response'];
        return (!is_array($user)) 
                ? (string) "not found"
                : (array) $user[0];
    }
}
?>