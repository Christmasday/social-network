<?php
// Database Libraries
require_once('DB/rb.php');
require_once('DB/jsonDB.php');
require_once('DB/DBManager.php');
require_once('DB/JSONManager.php');
// Security Libraries
require_once("Security/cryptography/SecureData.php");
require_once("Security/cryptography/LibCrypt.php");
require_once("Security/cryptography/Crypt.php");
require_once("Security/authentication/token.php");
require_once("Security/authentication/VerifyEmail.php");
// Routing Library
require_once('Routing/routing.php');
// Uploader Counter Library
require_once('Uploader/BulletProof.php');


const DB_TYPE = 'nosql';


?>