<?php

Trait Response
{
    private static $statuscodes = array(
            100 => 'Continue',  
            101 => 'Switching Protocols',  
            200 => 'OK',
            201 => 'Created',  
            202 => 'Accepted',  
            203 => 'Non-Authoritative Information',  
            204 => 'No Content',  
            205 => 'Reset Content',  
            206 => 'Partial Content',  
            300 => 'Multiple Choices',  
            301 => 'Moved Permanently',  
            302 => 'Found',  
            303 => 'See Other',  
            304 => 'Not Modified',  
            305 => 'Use Proxy',  
            306 => '(Unused)',  
            307 => 'Temporary Redirect',  
            400 => 'Bad Request',  
            401 => 'Unauthorized',  
            402 => 'Payment Required',  
            403 => 'Forbidden',  
            404 => 'Not Found',  
            405 => 'Method Not Allowed',  
            406 => 'Not Acceptable',  
            407 => 'Proxy Authentication Required',  
            408 => 'Request Timeout',  
            409 => 'Conflict',  
            410 => 'Gone',  
            411 => 'Length Required',  
            412 => 'Precondition Failed',  
            413 => 'Request Entity Too Large',  
            414 => 'Request-URI Too Long',  
            415 => 'Unsupported Media Type',  
            416 => 'Requested Range Not Satisfiable',  
            417 => 'Expectation Failed',  
            500 => 'Internal Server Error',  
            501 => 'Not Implemented',  
            502 => 'Bad Gateway',  
            503 => 'Service Unavailable',  
            504 => 'Gateway Timeout',  
            505 => 'HTTP Version Not Supported'
        );

    public static function json($status = 404, $data)
    {        
        @ header("HTTP/1.1 ".(int)$status." ".self::$statuscodes[$status]);
        self::recordRequest(
            $status, 
            self::$statuscodes[$status], 
            (is_string($data) || $_SERVER['REQUEST_METHOD'] !== 'GET') ? $data : "Request successful"
        );
        return (string)json_encode(
            array(
                "message" => (string)self::$statuscodes[$status],
                ($status == 200) ? 'response' : "error" => $data,
            ),
            JSON_PRETTY_PRINT
        );
    }

    private static function firebaseConnect($controls = null, $data = array())
    {
        FIREBASESDK::init();
        switch($_SERVER['REQUEST_METHOD'])
        {
            case "POST":
            {
                echo FIREBASESDK::getData(strtolower($controls), $data);
                break;
            }
        }
    }
    
    public static function fInclude($dir = false)
    {
        // If the directory does not exist, just skip it
        if(!is_dir($dir))
            return $this;
        // Scan the folder you want to include files from
        $files  =   scandir($dir);
        // If there are no files, just return
        if(empty($files))
            return false;
        // Loop through the files found
        foreach($files as $file) {
            // Include the directory
            $include    =   str_replace("//","/","{$dir}/{$file}");
            // If the file is a php document, include it
            if(is_file($include) && preg_match('/.*\.php$/',$include))
                include_once($include);
        }
        // Return the method just so you can chain it.
        return @ $this;
    }

    public static function cors()
    {
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');
        if(in_array($_SERVER['REQUEST_METHOD'], array('POST', 'GET', 'DELETE', 'PUT')))
        {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        }
        else if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') 
        {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            exit(0);
        }
    }

    private static function recordRequest($status = null, $msg = null, $data = null)
    {
        $time = date("G:i:s", time());
        switch(DB_TYPE)
        {
            case "sql":
            {
                DBManager::connect();
                DBManager::$table = 'servicelogs';
                DBManager::$data = array(
                    "status" => $status,
                    "method" => $_SERVER['REQUEST_METHOD'],
                    "url" => $_GET['controller'],
                    "response" => $data,
                    'time' => $time,
                    'year' => date("y"),
                    'month' => date("m"),
                    'day' => date("d"),
                );
                DBManager::Add();
                DBManager::disconnect();
                break;
            }
            case "nosql":
            {
                JSONManager::processJsonDB(
                    'servicelogs',
                    '',
                    array(
                        "status" => $status,
                        "method" => $_SERVER['REQUEST_METHOD'],
                        "url" => $_GET['controller'],
                        "response" => $data,
                        'time' => $time,
                        'year' => date("y"),
                        'month' => date("m"),
                        'day' => date("d"),
                    )
                );
                JSONManager::addData();
                break;
            }
        }
    }
}


?>