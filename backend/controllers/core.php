<?php
include_once('../model/libs/engine.php');
include_once('../model/libs/libs.php');
include_once('IController.php');
include_once('../view/Response.php');
define('Path', '../');
@ Response::cors();

Abstract Class CoreController
{
    private static $status = null, $response = null;

    // authorize user function
    private static function authUser()
    {
        // Token::verifyToken('Guest', DataSecurity::$headers['jwt'], DataSecurity::$headers['x-token'], DataSecurity::$headers['csrf']);
        return ($_SERVER['REQUEST_METHOD'] === "GET" && $_REQUEST['controller'] == 'accounts/guest')
                ? true
                : false;
    }

    private static function getController()
    {
        $route = ucfirst(explode('/', $_GET['controller'])[0]); 
        return (file_exists('modules/'.$route.'.php'))  // Checks if the module exists
                ? self::includeControls($route)
                : (file_exists('controls/'.$route.'.php'))  // Checks if the controller exists
                    ? self::includeControls($route)
                    : Response::json(
                        404,
                        "Modules {$route} not found" 
                    );
    }

    private function includeControls($controls = null)
    {
        require_once('controls/'.$controls.'.php');
        return $controls::processModule(@explode('/', $_GET['controller'])[1]);
    }

    // are modules for api that can contain various controls
    private static function includeModule($modules = null)
    {
        require_once($modules);
        return $modules::processModule(@explode('/', $_GET['controller'])[1]);
    }

    public static function Output()
    {
        // if(self::authUser())
        //     return Response::json(
        //         403,
        //         "Access denied to server. Authorization required" 
        //     );

        // check if controller is empty
        return (@ empty(explode('/', $_GET['controller'])[0]) || @ !isset($_GET['controller'])) 
            ? Response::json(
                400,
                "Bad request made to server" 
              )
            : self::getController();
    }
}

try
{
    $result = CoreController::Output();
}
catch(Exception $ex)
{
    $result = Response::json(
        500,
        "Error :".$ex->getMessage()." occured" 
    );
}
echo $result;
exit();
?>