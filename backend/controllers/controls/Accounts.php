<?php
Abstract Class Accounts
{
    private static $entities = array("fname", "lname", "mname", "sex", 'status', "email", "mobile", "address", 'authid', 'access'),
                    $auth =array("username", "password", "authid", "status", "access");

    static function processModule($func = null)
    {
        include_once('../model/DBM.php');
        $func = strtolower($func);
        DBManager::$table = strtolower(__CLASS__);
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            DBManager::$key[0] = "authid";
            DBManager::$key[1] = "access";
            DBManager::$key[2] = "username";
            $_POST['authid'] = base64_encode($_POST['username']);
        }
        Manager::$map = self::$entities;
        return (in_array($func, get_class_methods(__CLASS__)))
                ? self::$func()
                : Manager::init();
    }

    static function Access()
    {
        Manager::$map = array();
        return Rbac::rbacinit();
    }

    final private function login()
    {
        // if($_SERVER['REQUEST_METHOD'] !== 'POST')
        //     return Response::json(405, "Request method not supportted by request made. Please change your request method");
            
        $data = AuthModel::getAuthData($_POST);
        return (is_string($auth))
                ? Response::json('400', 'User does not exist. Please register')
                : AuthModel::validate($_POST['password'], $data, SALT);
    }

    final private function register()
    {
        // if($_SERVER['REQUEST_METHOD'] !== 'POST')
        //     return Response::json(405, "Request method not supportted by request made. Please change your request method");
            
        DBManager::$table = strtolower(DBManager::$table."auth");
        $_POST['password'] = _create_hash($_POST['password'], SALT);
        $_REQUEST['controller'] = $_GET['controller'] = 'accounts';
        foreach(DBManager::$key as $index => $values)
        {
            $_GET['controller'] .= '/'.$values.'/'.$arr[$values];
        }
        $_REQUEST['controller'] = $_GET['controller'];
        Manager::$map = self::$auth;
        return Manager::init();
    }

    private static function guest()
    {
        if($_SERVER['REQUEST_METHOD'] !== 'POST')
            return Response::json(405, "Request method not supportted by request made. Please change your request method");

        return Response::json(200, 
                        array(
                            "token" => Token::generateToken("Guest", @ $GLOBALS['HEADERS']['X-Token']), 
                            "data" => array(
                                "username" => "John Doe",
                                "access" => "Guest",
                                "status" => "activated"
                            ),
                        )
                    );
    }
    
    static function forgotpassword()
    {
        // if($_SERVER['REQUEST_METHOD'] !== 'PUT')
        //     return Response::json(405, "Request method not supportted by request made. Please change your request method");
            
        DBManager::$table = strtolower(DBManager::$table."auth");
        $data = AuthModel::getAuthData($_POST);
        $_REQUEST['controller'] = $_GET['controller'] = 'accounts';
        foreach(DBManager::$key as $index => $values)
        {
            $_GET['controller'] .= '/'.$values.'/'.$arr[$values];
        }
        $_REQUEST['controller'] = $_GET['controller'];
        Manager::$map = self::$auth;
        return Manager::init();
    }
    
    static function Remove()
    {
        // if($_SERVER['REQUEST_METHOD'] !== 'DELETE')
        //     return Response::json(405, "Request method not supportted by request made. Please change your request method");
            
        DBManager::$table = strtolower(DBManager::$table."auth");
        $key = explode('/', $_GET['controller']);
        if(count($key) < 2 || count($key) < 3 || count($key) < 4)
            return Response::json(400, "Invalid request made to server.");

        unset($key[1]);
        $_REQUEST['controller'] = $_GET['controller'] = implode('/', $key);
        return Manager::init();
    }
}

?>