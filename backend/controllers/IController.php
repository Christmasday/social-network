<?php

Interface IController
{
    static function processModule();
}

Interface IModules
{
    static function checkController();
    static function processModule();
}

?>